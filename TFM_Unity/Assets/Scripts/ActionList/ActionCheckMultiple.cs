﻿

using UnityEngine;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace HighSchool
{

	
	[System.Serializable]
	public class ActionCheckMultiple : Action
	{

		public int numSockets = 2;
		public override int NumSockets { get { return numSockets; }}

	}
	
}