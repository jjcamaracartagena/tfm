﻿
namespace HighSchool
{

	/** An interface used to aid the location of references to ActionListAsset files */
	public interface iActionListAssetReferencer
	{

		#if UNITY_EDITOR

		bool ReferencesAsset (ActionListAsset actionListAsset);

		#endif

	}

}