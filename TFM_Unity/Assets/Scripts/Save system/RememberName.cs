﻿

using UnityEngine;

namespace HighSchool
{

	/**
	 * This script is attached to GameObject in the scene whose change in name we wish to save.
	 */
	[AddComponentMenu("High School/Save system/Remember Name")]
	public class RememberName : Remember
	{

		public override string SaveData ()
		{
			NameData nameData = new NameData();
			nameData.objectID = constantID;
			nameData.savePrevented = savePrevented;

			nameData.newName = gameObject.name;

			return Serializer.SaveScriptData <NameData> (nameData);
		}


		public override void LoadData (string stringData)
		{
			NameData data = Serializer.LoadScriptData <NameData> (stringData);
			if (data == null) return;
			SavePrevented = data.savePrevented; if (savePrevented) return;

			gameObject.name = data.newName;
		}

	}


	/**
	 * A data container used by the RememberName script.
	 */
	[System.Serializable]
	public class NameData : RememberData
	{

		/** The GameObject's new name */
		public string newName;

		/**
		 * The default Constructor.
		 */
		public NameData () { }

	}

}