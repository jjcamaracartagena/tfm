﻿

using UnityEngine;

namespace HighSchool
{

	/**
	 * This script disables the Renderer component of any GameObject it is attached to, making it invisible.
	 */
	public class Invisible : MonoBehaviour
	{

		#region Variables

		public bool affectOwnGameObject = true;
		public enum ChildrenToAffect { None, OnlyActive, All };
		public ChildrenToAffect childrenToAffect;

		#endregion


		#region UnityStandards
		
		protected void Awake ()
		{
			Renderer ownRenderer = GetComponent <Renderer>();
			Renderer[] allRenderers = new Renderer[1];
			allRenderers[0] = ownRenderer;

			if (childrenToAffect != ChildrenToAffect.None)
			{
				bool includeInactive = (childrenToAffect == ChildrenToAffect.All);
				allRenderers = GetComponentsInChildren <Renderer>(includeInactive);
			}

			foreach (Renderer _renderer in allRenderers)
			{
				if (_renderer && _renderer == ownRenderer && !affectOwnGameObject)
				{
					continue;
				}
				_renderer.enabled = false;
			}
		}

		#endregion

	}

}