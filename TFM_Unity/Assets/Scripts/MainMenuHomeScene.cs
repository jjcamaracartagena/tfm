﻿using UnityEngine;
using UnityEngine.SceneManagement;


public class MainMenuHomeScene : MonoBehaviour {

  
	public GameObject Loading;

    void Awake(){
	
		if (Loading != null)
			Loading.SetActive (false);
		

    }

	public void LoadScene(string name){
		if (Loading != null) {

			SceneManager.LoadScene(name);
			//	Loading.SetActive (true);
			
		}
	}



    public void ExitGame()
    {

		Application.Quit();
		UnityEditor.EditorApplication.isPlaying = false;
	}


    public void Tutorial(){
		
		SceneManager.LoadScene ("Tutorial");
	}




}
