﻿

using UnityEngine;
using UnityEngine.Playables;
#if !ACIgnoreTimeline
using UnityEngine.Timeline;

namespace HighSchool
{

	/**
	 * A PlayableBehaviour used by CameraFadeMixer.
	 */
	internal sealed class CameraFadePlayableBehaviour : PlayableBehaviour
	{

		#region Variables

		public Texture2D overlayTexture;

		#endregion


		#region GetSet

		public bool IsValid
		{
			get
			{
				return overlayTexture != null;
			}
		}

		#endregion

	}

}
#endif