﻿

using UnityEngine;

namespace HighSchool
{

	/**
	 * An ActionList that is run when a Hotspot is clicked on.
	 */
	[System.Serializable]
	public class Interaction : ActionList
	{ }
	
}