﻿
using UnityEngine;

namespace HighSchool
{

	/**
	 * An ActionList that is run when a Conversation's dialogue option is clicked on, unless the Conversation has been overridden with the "Dialogue: Start conversation" Action.
	 */
	[System.Serializable]

	public class DialogueOption : ActionList
	{ }
	
}