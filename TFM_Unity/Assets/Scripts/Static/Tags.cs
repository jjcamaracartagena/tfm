﻿
namespace HighSchool
{

	/**
	 * Provides a number of tag names stored as const string variables.
	 */
	public class Tags
	{

		/** The name of the "MainCamera" tag */
		public const string mainCamera = "MainCamera";
		/** The name of the "Untagged" tag */
		public const string untagged = "Untagged";

	}

}