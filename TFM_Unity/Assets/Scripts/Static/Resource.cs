﻿
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace HighSchool
{

	public class Resource
	{

		// Main Reference resource
		private const string referencesName = "References";

		// Created by Kickstarter on Awake
		public const string persistentEngine = "PersistentEngine";

		// Created by BackgroundImageUI singleton
		private const string backgroundImageUIName = "BackgroundImageUI";

		// Used by DragTracks
		private const string dragColliderName = "DragCollider";

		// Used by UISlot
		private const string emptySlotName = "EmptySlot";

		// External links
		public const string manualLink = "https://www.org/files/.pdf";
		public const string assetLink = "https://assetstore.unity.com/packages/templates/systems/g-g-11896";
		public const string websiteLink = "https://g.org/";
		public const string tutorialsLink = "https://www.h.org/tutorials/";
		public const string downloadsLink = "https://www.g.org/downloads/";
		public const string forumLink = "https://www.g.org/forum/";
		public const string scriptingGuideLink = "https://www.g.org/scripting-guide/";
		public const string wikiLink = "http://g-g.wikia.com/wiki/";
		public const string introTutorialLink = "https://www.g.org/tutorials/game-editor-window";


		private static Collider dragColliderPrefab;
		public static Collider DragCollider
		{
			get
			{
				if (dragColliderPrefab == null)
				{
					dragColliderPrefab = (Collider) Resources.Load (dragColliderName, typeof (Collider));
				}
				return dragColliderPrefab;
			}
		}


		private static Sprite emptySlotAsset;
		public static Sprite EmptySlot
		{
			get
			{
				if (emptySlotAsset == null)
				{
					emptySlotAsset = (Sprite) Resources.Load (emptySlotName, typeof (Sprite));
				}
				return emptySlotAsset;
			}
		}


		private static GameObject backgroundImageUIPrefab;
		public static GameObject BackgroundImageUI
		{
			get
			{
				if (backgroundImageUIPrefab == null)
				{
					backgroundImageUIPrefab = (GameObject) Resources.Load (backgroundImageUIName, typeof (GameObject));
				}
				return backgroundImageUIPrefab;
			}
		}


		private static References referencesAsset;
		public static References References
		{
			get
			{
				if (referencesAsset == null)
				{
					referencesAsset = (References) Resources.Load (referencesName, typeof (References));

					if (referencesAsset == null)
					{
						References[] allReferences = Resources.FindObjectsOfTypeAll (typeof (References)) as References[];
						if (allReferences.Length > 0) referencesAsset = allReferences[0];
					}
				}
				return referencesAsset;
			}
			set
			{
				referencesAsset = value;
			}
		}


		#if UNITY_EDITOR

		private static Texture2D acLogo;
		private static Texture2D greyTexture;
		private static GUISkin nodeSkin;


		public static Texture2D HSLogo
		{
			get
			{
				if (acLogo == null)
				{
					acLogo = (Texture2D) AssetDatabase.LoadAssetAtPath (MainFolderPath + "/Graphics/Textures/logo.png", typeof (Texture2D));
					if (acLogo == null)
					{
						HSDebug.LogWarning ("Cannot find Texture asset file '" + MainFolderPath + "/Graphics/Textures/logo.png'");
					}
				}
				return acLogo;
			}
		}


		public static Texture2D GreyTexture
		{
			get
			{
				if (greyTexture == null)
				{
					greyTexture = (Texture2D) AssetDatabase.LoadAssetAtPath (MainFolderPath + "/Graphics/Textures/grey.png", typeof (Texture2D));
					if (greyTexture == null)
					{
						HSDebug.LogWarning ("Cannot find Texture asset file '" + MainFolderPath + "/Graphics/Textures/grey.png'");
					}
				}
				return greyTexture;
			}
		}


		public static GUISkin NodeSkin
		{
			get
			{
				if (nodeSkin == null)
				{
					nodeSkin = (GUISkin) AssetDatabase.LoadAssetAtPath (MainFolderPath + "/Graphics/Skins/ACNodeSkin.guiskin", typeof (GUISkin));
					if (nodeSkin == null)
					{
						HSDebug.LogWarning ("Cannot find GUISkin asset file '" + MainFolderPath + "/Graphics/Skins/ACNodeSkin.guiskin'");
					}
				}
				return nodeSkin;
			}
		}


		
		public static string MainFolderPath
		{
			get
			{
			//	Debug.Log("Esta es la carpeta: " + HSEditorPrefs.InstallPath);
			//	return HSEditorPrefs.InstallPath;
				return "Assets/Arts";
			}
		}


		public static string DefaultReferencesPath
		{
			get
			{
				return MainFolderPath + "/Resources";
			}
		}


		public static string DefaultActionsPath
		{
			get
			{
				return "Assets/Scripts/Actions";
			}
		}

		#endif

	}

}