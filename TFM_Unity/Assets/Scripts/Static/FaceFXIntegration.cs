﻿

using UnityEngine;


namespace HighSchool
{

	/**
	 * A class the contains a number of static functions to assist with FaceFX integration.
	 * To use FaceFX with High School, the 'FaceFXIsPresent' preprocessor must be defined.
	 */
	public class FaceFXIntegration
	{

		/**
		 * <summary>Checks if the 'FaceFXIsPresent' preprocessor has been defined.</summary>
		 * <returns>True if the 'FaceFXIsPresent' preprocessor has been defined</returns>
		 */
		public static bool IsDefinePresent ()
		{
			#if FaceFXIsPresent
			return true;
			#else
			return false;
			#endif
		}
		

		/**
		 * <summary>Plays a FaceFX animation on a character, based on an AudioClip.</summary>
		 * <param name = "speaker">The speaking character</param>
		 * <param name = "name">The unique identifier of the line in the format Joe13, where 'Joe' is the name of the character, and '13' is the ID number of ths speech line</param>
		 * <param name = "audioClip">The speech AudioClip</param>
		 */
		public static void Play (HighSchool.Char speaker, string name, AudioClip audioClip)
		{
			#if FaceFXIsPresent
			FaceFXControllerScript_Base fcs = speaker.GetComponent <FaceFXControllerScript_Base>();
			if (fcs == null)
			{
				fcs = speaker.GetComponentInChildren <FaceFXControllerScript_Base>();
			}
			if (fcs != null)
			{
				speaker.isLipSyncing = true;
				fcs.PlayAnim ("Default_" + name, audioClip);
			}
			else
			{
				HSDebug.LogWarning ("No FaceFXControllerScript_Base script found on " + speaker.gameObject.name);
			}
			#else
			HSDebug.LogWarning ("The 'FaceFXIsPresent' preprocessor define must be declared in the Player Settings.");
			#endif
		}


		/**
		 * <summary>Stops the FaceFX animation on a character.</summary>
		 * <param name = "speaker">The speaking character</param>
		 */
		public static void Stop (HighSchool.Char speaker)
		{
			#if FaceFXIsPresent
			FaceFXControllerScript_Base fcs = speaker.GetComponent <FaceFXControllerScript_Base>();
			if (fcs == null)
			{
				fcs = speaker.GetComponentInChildren <FaceFXControllerScript_Base>();
			}
			if (fcs != null)
			{
				fcs.StopAnim ();
			}
			else
			{
				HSDebug.LogWarning ("No FaceFXControllerScript_Base script found on " + speaker.gameObject.name);
			}
			#else
			HSDebug.LogWarning ("The 'FaceFXIsPresent' preprocessor define must be declared in the Player Settings.");
			#endif
		}
		
	}
	
}