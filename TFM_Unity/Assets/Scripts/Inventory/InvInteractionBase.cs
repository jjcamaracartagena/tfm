﻿
using UnityEngine;
using System.Collections.Generic;

namespace HighSchool
{

	/** A base class for inventory interactions.. */
	[System.Serializable]
	public abstract class InvInteractionBase
	{

		#region Variables

		/** The ActionList to run when the interaction is triggered */
		public ActionListAsset actionList;
		[SerializeField] protected int idPlusOne;

		#endregion


		#region GetSet

		/** A unique identifier */
		public int ID
		{
			get
			{
				return idPlusOne - 1;
			}
			set
			{
				idPlusOne = value + 1;
			}
		}

		#endregion

	}

}