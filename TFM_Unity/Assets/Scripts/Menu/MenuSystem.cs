﻿

using UnityEngine;

namespace HighSchool
{

	public class MenuSystem : MonoBehaviour
	{

		public static void OnMenuEnable (HighSchool.Menu _menu)
		{
			// This function is called whenever a menu is enabled.

			if (_menu.title == "Pause")
			{
				MenuElement saveButton = _menu.GetElementWithName ("SaveButton");
				
				if (saveButton)
				{
					saveButton.IsVisible = !PlayerMenus.IsSavingLocked ();
				}
				
				_menu.Recalculate ();
			}
		}
		

		public static void OnElementClick (HighSchool.Menu _menu, MenuElement _element, int _slot, int _buttonPressed)
		{
			// This function is called whenever a clickable element has a click type of "Custom Script".
		}
		
	}

}