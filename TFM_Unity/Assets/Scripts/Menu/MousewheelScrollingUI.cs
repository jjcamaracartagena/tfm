﻿
 
using UnityEngine;

namespace HighSchool
{

	/** This script, when attached to the root canvas of a Unity UI linked to  's Menu Manager, allows you to scroll through the slots of a given element with an input (by default, the mouse scrollwheel) */
	[AddComponentMenu ("High School/UI/Mousewheel scrolling UI")]
	public class MousewheelScrollingUI : MonoBehaviour
	{

		#region Variables

		[SerializeField] private string inputName = "Mouse ScrollWheel";
		[SerializeField] private string elementToScroll = "";

		private MenuElement element;

		private bool allowScrollWheel;
		private float inputThreshold = 0.05f;

		#endregion


		#region UnityStandards

		private void OnEnable ()
		{
			if (element == null && KickStarter.playerMenus)
			{
				Menu menu = KickStarter.playerMenus.GetMenuWithCanvas (GetComponent <Canvas>());
				if (menu != null && !string.IsNullOrEmpty (elementToScroll))
				{
					element = menu.GetElementWithName (elementToScroll);
				}
			}
		}


		private void Update ()
		{
			if (element == null) return;

			float scrollInput = 0f;
			try
			{
				scrollInput = Input.GetAxisRaw (inputName);
			}
			catch
			{ }

			if (scrollInput > inputThreshold)
			{
				if (allowScrollWheel)
				{
					element.Shift (HS_ShiftInventory.ShiftPrevious, 1);
					allowScrollWheel = false;
				}
			}
			else if (scrollInput < -inputThreshold)
			{
				if (allowScrollWheel)
				{
					element.Shift (HS_ShiftInventory.ShiftNext, 1);
					allowScrollWheel = false;
				}
			}
			else
			{
				allowScrollWheel = true;
			}
		}

		#endregion
	}

}