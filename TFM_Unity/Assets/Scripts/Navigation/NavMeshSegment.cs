﻿
using UnityEngine;

namespace HighSchool
{

	/**
	 * Controls a navigation area used by Unity Navigation-based pathfinding method.
	 */

	[AddComponentMenu("High School/Navigation/NavMesh Segment")]
	public class NavMeshSegment : NavMeshBase
	{

		#region UnityStandards

		protected void Awake ()
		{
			BaseAwake ();

			if (KickStarter.sceneSettings.navigationMethod == HS_NavigationMethod.UnityNavigation)
			{
				if (LayerMask.NameToLayer (KickStarter.settingsManager.navMeshLayer) == -1)
				{
					HSDebug.LogWarning ("No 'NavMesh' layer exists - please define one in the Tags Manager.");
				}
				else
				{
					gameObject.layer = LayerMask.NameToLayer (KickStarter.settingsManager.navMeshLayer);
				}
			}
		}

		#endregion

	}

}