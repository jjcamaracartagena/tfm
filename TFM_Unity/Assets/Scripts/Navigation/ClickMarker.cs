﻿

using UnityEngine;
using System.Collections;

namespace HighSchool
{


	public class ClickMarker : MonoBehaviour
	{

		#region Variables

		/** How long the marker will remain visible on-screen */
		public float lifeTime = 0.5f;

		protected float startTime;
		protected Vector3 startScale;
		protected Vector3 endScale = Vector3.zero;

		#endregion


		#region UnityStandards

		protected void Start ()
		{
			Destroy (this.gameObject, lifeTime);

			if (lifeTime > 0f)
			{
				startTime = Time.time;
				startScale = transform.localScale;
			}

			StartCoroutine ("ShrinkMarker");
		}

		#endregion


		#region ProtectedFunctions

		protected IEnumerator ShrinkMarker ()
		{
			while (lifeTime > 0f)
			{
				transform.localScale = Vector3.Lerp (startScale, endScale, AdvGame.Interpolate (startTime, lifeTime, MoveMethod.EaseIn, null));
				yield return new WaitForFixedUpdate ();
			}
		}

		#endregion

	}

}