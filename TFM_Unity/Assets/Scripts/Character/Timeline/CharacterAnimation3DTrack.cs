﻿
#if !ACIgnoreTimeline

using UnityEngine.Timeline;

namespace HighSchool
{

	[TrackClipType (typeof (CharacterAnimation3DShot))]
	#if UNITY_2018_3_OR_NEWER
	[TrackBindingType (typeof (HighSchool.Char), TrackBindingFlags.None)]
	#else
	[TrackBindingType (typeof (HighSchool.Char))]
	#endif
	[TrackColor (0.2f, 0.6f, 0.9f)]
	public class CharacterAnimation3DTrack : TrackAsset
	{}

}

#endif