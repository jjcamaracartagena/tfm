﻿

#if !ACIgnoreTimeline

using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;


namespace HighSchool
{

	[System.Serializable]
	[TrackClipType (typeof (HeadTurnShot))]
	[TrackColor (0.1f, 0.1f, 0.73f)]
	#if UNITY_2018_3_OR_NEWER
	[TrackBindingType (typeof (HighSchool.Char), TrackBindingFlags.None)]
	#else
	[TrackBindingType(typeof(HighSchool.Char))]
	#endif
	/**
	 * A TrackAsset used by HeadTurnMixer.
	 */
	public class HeadTurnTrack : TrackAsset
	{

		#region PublicFunctions

		public override Playable CreateTrackMixer (PlayableGraph graph, GameObject go, int inputCount)
		{
			ScriptPlayable<HeadTurnMixer> mixer = ScriptPlayable<HeadTurnMixer>.Create (graph);
			mixer.SetInputCount (inputCount);
			return mixer;
		}

		#endregion

	}

}

#endif