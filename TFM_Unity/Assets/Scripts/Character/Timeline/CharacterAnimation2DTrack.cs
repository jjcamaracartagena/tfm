﻿
#if !ACIgnoreTimeline

using UnityEngine.Timeline;

namespace HighSchool
{

	[TrackClipType (typeof (CharacterAnimation2DShot))]
	#if UNITY_2018_3_OR_NEWER
	[TrackBindingType (typeof (HighSchool.Char), TrackBindingFlags.None)]
	#else
	[TrackBindingType (typeof (HighSchool.Char))]
	#endif
	[TrackColor (0.2f, 0.6f, 0.9f)]
	public class CharacterAnimation2DTrack : TrackAsset
	{}

}

#endif