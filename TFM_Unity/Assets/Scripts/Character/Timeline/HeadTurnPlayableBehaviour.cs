﻿
#if !ACIgnoreTimeline

using UnityEngine;
using UnityEngine.Playables;

namespace HighSchool
{

	/**
	 * A PlayableBehaviour used by HeadTurnMixer.
	 */
	internal sealed class HeadTurnPlayableBehaviour : PlayableBehaviour
	{

		#region Variables

		public Transform headTurnTarget;
		public Vector3 headTurnOffset;

		#endregion


		#region GetSet

		public bool IsValid
		{
			get
			{
				return headTurnTarget != null;
			}
		}

		#endregion

	}

}
#endif