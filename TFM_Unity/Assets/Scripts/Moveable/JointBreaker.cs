﻿
using UnityEngine;

namespace HighSchool
{

	/**
	 * This component is used by PickUp to clean up FixedJoints after they've broken.
	 */
	public class JointBreaker : MonoBehaviour
	{

		#region Variables

		protected FixedJoint fixedJoint;

		#endregion


		#region UnityStandards

		protected void Awake ()
		{
			fixedJoint = GetComponent <FixedJoint>();
		}


		protected void OnJointBreak (float breakForce)
		{
			fixedJoint.connectedBody.GetComponent <Moveable_PickUp>().UnsetFixedJoint ();
			Destroy (this.gameObject);
		}

		#endregion

	}

}